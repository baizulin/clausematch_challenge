(function () {
    'use strict';

    module.exports = function (grunt) {
        grunt.initConfig({
            pkg: grunt.file.readJSON('package.json'),

            clean  : {
                options: {
                    force: true
                },
                build  : ['assets']
            },
            less   : {
                development: {
                    options: {
                        paths: ["src/less/"]
                    },
                    files  : {
                        'assets/css/main.css': 'src/less/main.less'
                    }
                },
                production : {
                    options: {
                        paths   : ["src/less/"],
                        compress: true,
                        cleancss: true
                    },
                    files  : {
                        'assets/css/main.min.css': 'src/less/main.less'
                    }
                }
            },
            postcss: {
                options: {
                    processors: [
                        require('autoprefixer')({ browsers: ['last 2 version'] })
                    ]
                },
                styles : {
                    src: 'assets/css/main*.css'
                }
            },
            copy   : {
                jsVendors: {
                    expand: true,
                    flatten: true,
                    src : 'vendor/js/*',
                    dest: 'assets/js/'
                },
                cssVendors: {
                    expand: true,
                    flatten: true,
                    src : 'vendor/css/*',
                    dest: 'assets/css/'
                },
                js: {
                    expand: true,
                    flatten: true,
                    src : 'src/js/*',
                    dest: 'assets/js/'
                }
            },
            watch  : {
                less: {
                    files  : ['src/less/*.less'],
                    tasks  : ['less'],
                    options: {
                        spawn     : false,
                        livereload: true
                    }
                },
                js  : {
                    files  : ['src/js/**/*.js'],
                    tasks  : ['copy:js'],
                    options: {
                        spawn     : false,
                        livereload: true
                    }
                }
            }
        });

        grunt.loadNpmTasks('grunt-contrib-clean');
        grunt.loadNpmTasks('grunt-contrib-less');
        grunt.loadNpmTasks('grunt-postcss');
        grunt.loadNpmTasks('grunt-contrib-copy');
        grunt.loadNpmTasks('grunt-contrib-watch');

        grunt.registerTask('build', ['clean', 'less', 'postcss', 'copy']);
        grunt.registerTask('develop', ['build', 'watch']);
    };
})();
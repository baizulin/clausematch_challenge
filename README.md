# README #
ClauseMatch coding challenge solution by [Dennis Baizulin](https://vk.com/baizulin.dennis)

---

### Requirements ###
* Node.js
* npm

---

### Setup ###
Install npm dependencies

    npm install

---

### Run ###
Open `index.html` in any browser, except Firefox

---

### Disclaimer ###
1. Firefox has an issue with `Ctrl+B` and `Ctrl+I` combinations serving as predefined browser shortcuts. The challenge had no browser requirements, so screw it.
2. The challenge had no design and code requirements so I went with extremely minimalistic approach.

---

### Licence ###
[Creative Commons Attribution NonCommercial NoDerivatives 4.0 International Public License](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode)
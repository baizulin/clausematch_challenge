(function (angular, undefined) {
    'use strict';

    var app = angular.module('app', ['list']);

    app.config([
        '$compileProvider',
        function ($compileProvider) {
            $compileProvider.debugInfoEnabled(false);
            $compileProvider.commentDirectivesEnabled(false);
            $compileProvider.cssClassDirectivesEnabled(false);
        }
    ]);

    app.factory('listStorageService', function () {
        return {
            data: [
                '<i>Hello</i> world...<br><br>This is some content<br><br>Isn\'t it awesome?<br><br><b>I like it...</b>'
            ]
        };
    });

    app.controller('MainController', [
        '$rootScope', 'listStorageService',
        function ($rootScope, listStorageService) {
            this.list = listStorageService;
        }
    ]);
})(window.angular);

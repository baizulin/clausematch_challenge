(function (angular, angularDragula, undefined) {
    'use strict';

    var list = angular.module('list', [angularDragula(angular)]);

    list.directive('contenteditable', function () {
        return {
            restrict: 'A',
            require : 'ngModel',
            link    : function ($scope, $element, attrs, ngModel) {
                ngModel.$render = function () {
                    $element.html(ngModel.$viewValue);
                };

                $element.on('input', function () {
                    ngModel.$setViewValue($element.html());
                });
            }
        };
    });

    list.controller('ListController', [
        '$scope', 'dragulaService',
        function ($scope, dragulaService) {
            dragulaService.options($scope, 'list', {
                moves: function (el, source, handle) {
                    return handle.dataset.hasOwnProperty('handle');
                }
            });

            this.canRemoveItems = function () {
                return this.data.length > 1;
            };

            this.notify = function () {
                this.onUpdate({ data: this.data });
            };

            this.set = function (index, value) {
                this.data[index] = value;
                this.notify();
            };

            this.createAfter = function (index) {
                this.data.splice(index + 1, 0, '');
            };

            this.remove = function (index) {
                if (this.canRemoveItems()) {
                    this.data.splice(index, 1);
                }
            };
        }
    ]);

    list.component('list', {
        bindings    : {
            data    : '<',
            onUpdate: '&'
        },
        controller  : 'ListController',
        controllerAs: 'list',
        template    : '\
            <div class="list" data-dragula=\'"list"\' data-dragula-model="list.data">\
                <div\
                    class="list__item list-item"\
                    data-ng-repeat="item in list.data track by $index"\
                >\
                    <div\
                        data-ng-model="item"\
                        contenteditable="true"\
                        class="list-item__editor"\
                        data-ng-change="list.set($index, item)"\
                    ></div>\
                    <div class="list-item__buttons cleafix">\
                        <div\
                            data-handle\
                            title="Reorder"\
                            class="list-item__reorder"\
                        >=</div>\
                        <div\
                            title="Add new item"\
                            class="list-item__add"\
                            data-ng-click="list.createAfter($index)"\
                        >+</div>\
                        <div\
                            title="Remove item"\
                            class="list-item__remove"\
                            data-ng-click="list.remove($index)"\
                            data-ng-disabled="!list.canRemoveItems()"\
                        >-</div>\
                    </div>\
                </div>\
            </div>'
    });
})(window.angular, window.angularDragula);